#!/usr/bin/env python3

from abc import ABC, abstractmethod
import argparse
import re
from typing import Dict, Iterator, List


class NodePrinter(ABC):
    @abstractmethod
    def __init__(self, nodes) -> None:
        super().__init__()
        self.nodes: List[Node] = nodes

    @abstractmethod
    def print_node(self, node: "Node") -> None:
        ...


class Node:
    TYPE_TARGET = 1
    TYPE_VAR = 2

    def __init__(self, name: str, type: int) -> None:
        self._name: str = name
        self._dependent_nodes: List[Node] = []
        self._type: int = type
        self._assigned: bool = False

    @property
    def name(self) -> str:
        return self._name

    @property
    def dependent_nodes(self) -> "Iterator[Node]":
        return iter(self._dependent_nodes)

    @property
    def type(self) -> int:
        return self._type

    @property
    def assigned(self) -> bool:
        return self._assigned

    @assigned.setter
    def assigned(self, new_value) -> None:
        self._assigned = new_value

    def print_to(self, printer: NodePrinter) -> None:
        printer.print_node(self)

    def add_dependency(self, node: "Node") -> None:
        if (node not in self._dependent_nodes):
            self._dependent_nodes.append(node)


class MakefilePreprocessor:
    define_matcher = re.compile(r"^define\s")
    endef_matcher = re.compile(r"^endef(\s|$)")

    def __init__(self, lines: List[str]) -> None:
        self.lines: List[str] = lines
        self.in_define: bool = False

    def process(self) -> None:
        processed_lines: List[str] = []
        current_line = ""
        for line in self.lines:
            if self.in_define:
                if MakefilePreprocessor.endef_matcher.match(line):
                    self.in_define = False
                continue
            if MakefilePreprocessor.define_matcher.match(line):
                self.in_define = True
                continue
            line = line.strip('\n')
            current_line += line
            if current_line and current_line[-1] != '\\':
                processed_lines.append(current_line)
                current_line = ""
            else:
                current_line = current_line[:-1]
        return processed_lines


class MakefileParser:
    start_rule_matcher = re.compile(r"([^:=]+):?:[^=].*")
    assignment_matcher = re.compile(r"^\s*([^:#=\s]+)\s*\+?\??:?=")
    variable_matcher = re.compile(r"\$\([^$():\s]+\)")
    variable_matcher2 = re.compile(r"\$\([^$():\s]+:")

    def __init__(self, makefile_lines: List[str]) -> None:
        self.makefile_lines = makefile_lines
        self.variable_name_to_node: Dict[str, Node] = {}

    def parse(self) -> List[Node]:
        current_target = "undefined"

        for line in self.makefile_lines:
            if not line or line[0] == '#':
                continue
            if line[0] == '\t':
                self.parse_variable_usage(
                    current_target, line, Node.TYPE_TARGET)
                continue
            m = MakefileParser.start_rule_matcher.match(line)
            if m:
                current_target = m[1].strip()
                self.parse_variable_usage(
                    current_target, line, Node.TYPE_TARGET)
                continue
            m = MakefileParser.assignment_matcher.match(line)
            if m:
                var_name = m[1].strip()
                node = self.parse_variable_usage(
                    var_name, line, Node.TYPE_VAR)
                node.assigned = True
        return self.variable_name_to_node.values()

    def get_node(self, name: str, type: int) -> Node:
        if (name, type) in self.variable_name_to_node:
            return self.variable_name_to_node[(name, type)]
        new_node = Node(name, type)
        self.variable_name_to_node[(name, type)] = new_node
        return new_node

    def parse_variable_usage(self, node_name, string, type) -> Node:
        variable_usages: List[str] = self.variable_matcher.findall(string)
        variable_usages2: List[str] = self.variable_matcher2.findall(string)
        variable_names = [
            var_string.strip('$():') for var_string in (variable_usages + variable_usages2)]
        node = self.get_node(node_name, type)
        for name in variable_names:
            other_node = self.get_node(name, Node.TYPE_VAR)
            node.add_dependency(other_node)
        return node


class NodesToDotFilePrinter(NodePrinter):
    def __init__(self, nodes: List[Node]) -> None:
        super().__init__(nodes)
        self.node_definition_lines: List[str] = []
        self.edge_definition_lines: List[str] = []
        self.target_layer_node_names = []

    def print_node(self, node: Node) -> None:
        attributes: List[str] = []
        if node.type == Node.TYPE_TARGET:
            attributes.append("shape=box")
        if node.type == Node.TYPE_VAR and not node.assigned:
            attributes.append("color=red")
        if attributes:
            self.node_definition_lines.append(
                f'"{node.name}" [{" ".join(attributes)}]')
        else:
            self.node_definition_lines.append(f'"{node.name}"')

        if node.dependent_nodes:
            for dependent_node in node.dependent_nodes:
                self.edge_definition_lines.append(
                    f'"{dependent_node.name}" -> "{node.name}"')

        if node.type == Node.TYPE_TARGET:
            self.target_layer_node_names.append(node.name)

    def print(self) -> None:
        for node in self.nodes:
            node.print_to(self)
        print("digraph D {")
        print("{")
        for line in self.node_definition_lines:
            print(line)
        print("}")
        print("{")
        print('rank=same')
        for name in self.target_layer_node_names:
            print(f'"{name}"')
        print("}")
        for line in self.edge_definition_lines:
            print(line)
        print("}")


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Create a directed graph in Graphviz dotfile format from a Makefile.")
    parser.add_argument("file", metavar="file", type=str, nargs='?',
                        help="Makefie file path and name (default: Makefile)", default="Makefile")

    args = parser.parse_args()

    with open(args.file, "r") as makefile:
        makefile_lines = MakefilePreprocessor(makefile).process()
    nodes = MakefileParser(makefile_lines).parse()
    printer = NodesToDotFilePrinter(nodes)
    printer.print()


if __name__ == "__main__":
    main()
